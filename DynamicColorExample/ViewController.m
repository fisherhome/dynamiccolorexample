//
//  ViewController.m
//  DynamicColorExample
//
//  Created by fisherman on 2021/12/29.
//

#import "ViewController.h"
#import "UIColor+Dynamic.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithDynamicLight:@"#4F3824" dark:@"#912a12"];
}


@end
