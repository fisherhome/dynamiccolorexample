//
//  UIColor+Dynamic.h
//  DynamicColor
//
//  Created by fisherman on 2021/12/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Dynamic)

+ (UIColor *)colorWithDynamicLight:(NSString *)lightColor dark:(NSString *)darkColor;

@end

NS_ASSUME_NONNULL_END

