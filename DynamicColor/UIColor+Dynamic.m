//
//  UIColor+Dynamic.m
//  DynamicColor
//
//  Created by fisherman on 2021/12/29.
//

#import "UIColor+Dynamic.h"
//#import <YYKit.h>

@implementation UIColor (Dynamic)

+ (UIColor *)colorWithDynamicLight:(NSString *)lightColor dark:(NSString *)darkColor {
    if (@available(iOS 13.0, *)) {
        return [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            if (traitCollection.userInterfaceStyle == UIUserInterfaceStyleLight) {
                return [UIColor dc_colorWithHexString:lightColor];
            } else if (traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
                return [UIColor dc_colorWithHexString:darkColor];
            } else {
                return UIColor.clearColor;
            }
        }];
    } else {
        return [UIColor dc_colorWithHexString:lightColor];
    }
}

static inline NSUInteger hexStrToInt(NSString *str) {
    uint32_t result = 0;
    sscanf([str UTF8String], "%X", &result);
    return result;
}

+ (UIColor *)dc_colorWithHexString:(NSString *)hexStr {
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    hexStr = [hexStr stringByTrimmingCharactersInSet:set];
    hexStr = [hexStr uppercaseString];
    if ([hexStr hasPrefix:@"#"]) {
        hexStr = [hexStr substringFromIndex:1];
    } else if ([hexStr hasPrefix:@"0X"]) {
        hexStr = [hexStr substringFromIndex:2];
    }
    if (hexStr.length == 6) {
        hexStr = [hexStr stringByAppendingString:@"FF"];
    }
    CGFloat r = hexStrToInt([hexStr substringWithRange:NSMakeRange(0, 2)]) / 255.0f;
    CGFloat g = hexStrToInt([hexStr substringWithRange:NSMakeRange(2, 2)]) / 255.0f;
    CGFloat b = hexStrToInt([hexStr substringWithRange:NSMakeRange(4, 2)]) / 255.0f;
    CGFloat a = hexStrToInt([hexStr substringWithRange:NSMakeRange(6, 2)]) / 255.0f;
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}
@end
